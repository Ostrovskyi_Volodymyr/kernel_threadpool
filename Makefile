KMOD=pool_test.ko
DIR=$(PWD)
BUILD_KERNEL=/lib/modules/`uname -r`/build
OBJS=src/threadpool.o src/pool_test.o
obj-m += pool_test.o
pool_test-objs:=$(OBJS)

all:
	make -C ${BUILD_KERNEL} M=${DIR} modules
	mkdir -p ${DIR}/bin
	mv ${DIR}/${KMOD} ${DIR}/bin/${KMOD}

clean:
	make -C ${BUILD_KERNEL} M=${DIR} clean