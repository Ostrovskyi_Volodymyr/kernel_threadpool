#include <linux/kernel.h>
#include <linux/module.h>

#include "threadpool.h"

static struct threadpool pool;
static const char *strs[5] = {
	"AAAAAA",
	"BBB",
	"CCCCCCC",
	"EEE",
	"FFFFF"
};

static void pool_job_handler(void *data);

static int __init drv_init(void) {
	printk(KERN_INFO "=== MODULE INIT ===\n");
	init_threadpool(&pool, 2);
	struct thread_job job;
	job.handler = pool_job_handler;
	size_t i = 0;
	for (; i < 5; i++) {
		job.data = strs[i];
		put_job(&pool, &job);
	}
	return 0;
}

static void __exit drv_exit(void) {
	destroy_threadpool(&pool);
	printk(KERN_INFO "=== MODULE EXIT ===\n");
}

static void pool_job_handler(void *data) {
	const char *str = (const char*)data;
	size_t i = 0;
	for (; i < 4; i++) {
		printk(KERN_INFO "%s\n", str);
		mdelay(100 * strlen(str));
	}
}

module_init(drv_init);
module_exit(drv_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("4umber");