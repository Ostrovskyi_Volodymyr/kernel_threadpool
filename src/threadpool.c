#include "threadpool.h"

#ifdef POOL_DEBUG
#define __POOL_LOG(...) printk(__VA_ARGS__)
#else
#define __POOL_LOG(...)
#endif

static void clear_thread_list(struct list_head *threads);
static void clear_job_list(struct list_head *jobs);
static int plthread_func(void *data);
static int pool_func(void *data);
static int put_job2thread(struct threadpool *pool, struct thread_job *job);
static void put_job2queue(struct threadpool *pool, struct thread_job *job);

void put_job(struct threadpool *pool, struct thread_job *job) {
	if (!put_job2thread(pool, job)) {
		__POOL_LOG(KERN_INFO "=== put job to queue ===\n");
		put_job2queue(pool, job);
	}
}

int init_threadpool(struct threadpool *pool, uint32_t thread_count) {
	pool->tcount = thread_count;
	INIT_LIST_HEAD(&(pool->threads));
	INIT_LIST_HEAD(&(pool->job_queue));
	spin_lock_init(&(pool->job_queue_lock));

	uint32_t i = 0;
	for (; i < thread_count; ++i) {
		struct plthread *t = kmalloc((size_t)sizeof(struct plthread), GFP_KERNEL);
		if (t == NULL) {
			clear_thread_list(&pool->threads);
			return -ENOMEM;
		}
		t->job.id = i;
		t->task = kthread_create(plthread_func, &(t->job), "plthread_%d", i);
		t->task->state = TASK_INTERRUPTIBLE;
		get_task_struct(t->task); // increment refcount
		list_add_tail(&(t->list), &(pool->threads));
	}
	pool->pool_task = kthread_create(pool_func, pool, "pool_th");
	get_task_struct(pool->pool_task); // increment refcount
	wake_up_process(pool->pool_task);
	return 0;
}

void destroy_threadpool(struct threadpool *pool) {
	kthread_stop(pool->pool_task);
	put_task_struct(pool->pool_task); // decrement refcount
	clear_thread_list(&pool->threads);
	clear_job_list(&pool->job_queue);
}

static int plthread_func(void *data) {
	struct thread_job *job = (struct thread_job*)data;

	while (!kthread_should_stop()) {
		unsigned long j = get_jiffies_64();
		__POOL_LOG(KERN_INFO "[JF: %lu | JID: %d] Start new job\n", j, job->id);
		job->handler(job->data);
		__POOL_LOG(KERN_INFO "[JF: %lu | JID: %d] End job\n", j, job->id);
		set_current_state(TASK_INTERRUPTIBLE);
		schedule();
	}
	return 0;
}

static int pool_func(void *data) {
	struct threadpool *pool = (struct threadpool*)data;
	while (!kthread_should_stop()) {
		if (!list_empty(&(pool->job_queue))) {
			struct thread_job *job = list_first_entry(&(pool->job_queue), struct thread_job, list);
			if (put_job2thread(pool, job)) {
				__POOL_LOG(KERN_INFO "=== get job from queue ===\n");
				spin_lock(&(pool->job_queue_lock));
				list_del(&(job->list));
				spin_unlock(&(pool->job_queue_lock));
				kfree(job);
			}
		}
		msleep(10);
	}
	return 0;
}

static int put_job2thread(struct threadpool *pool, struct thread_job *job) {
	struct plthread *entry;
	list_for_each_entry(entry, &(pool->threads), list) {
		if (task_state_index(entry->task) == TASK_INTERRUPTIBLE) {
			entry->job.handler = job->handler;
			entry->job.data = job->data;
			wake_up_process(entry->task);
			return 1;
		}
	}
	return 0;
}

static void put_job2queue(struct threadpool *pool, struct thread_job *job) {
	struct thread_job *qj = kmalloc((size_t)sizeof(struct thread_job), GFP_KERNEL);
	qj->handler = job->handler;
	qj->data = job->data;
	spin_lock(&(pool->job_queue_lock));
	list_add_tail(&(qj->list), &(pool->job_queue));
	spin_unlock(&(pool->job_queue_lock));
}

static void clear_thread_list(struct list_head *threads) {
	struct plthread *entry, *enext;
	list_for_each_entry_safe(entry, enext, threads, list) {
		list_del(&(entry->list));
		kthread_stop(entry->task);
		put_task_struct(entry->task); // decrement refcount
		kfree(entry);
		entry = enext;
	}
}

static void clear_job_list(struct list_head *jobs) {
	struct thread_job *entry, *enext;
	list_for_each_entry_safe(entry, enext, jobs, list) {
		list_del(&(entry->list));
		kfree(entry);
		entry = enext;
	}
}