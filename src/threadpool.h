#ifndef THREADPOOL_H_
#define THREADPOOL_H_

#include <linux/kernel.h>
#include <linux/list.h>
#include <linux/sched.h>
#include <linux/sched/task.h>
#include <linux/kthread.h>
#include <linux/spinlock.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/jiffies.h>

struct thread_job {
	void (*handler)(void *data);
	void *data;
	uint32_t id;
	struct list_head list;	
};

struct plthread {
	struct task_struct *task;
	struct thread_job job;
	struct list_head list;
};

struct threadpool {
	struct list_head threads;
	struct list_head job_queue;
	struct task_struct *pool_task;
	uint32_t tcount;
	spinlock_t job_queue_lock;
};

void put_job(struct threadpool *pool, struct thread_job *job);
int init_threadpool(struct threadpool *pool, uint32_t thread_count);
void destroy_threadpool(struct threadpool *pool);

#endif // THREADPOOL_H_
